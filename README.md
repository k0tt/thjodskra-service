thjodskra-service
=================

This service handles calls to Þjóðskrá API. To save external calls this API will store 
information from successful responses to a database and will return the lookup from the 
database if the same valid ssn is requested more than once.

Usage
=====

### Build and install
```stack build && stack install```

### Run
```thjodskra-service       (Runs on port 8081 by default)```

### Make a request
```GET http://localhost:8081/thjodskra/kennitala/2409872799```

### Response
```
{
    "kennitala": "2409872799",
    "postalName": "Reykjavík",
    "dateOfBirth": "24.09.1987",
    "postalCode": "xxx",
    "address": "xxxxxxxx",
    "name": "Þórður Guðmundur Hermannsson"
}
```

Environment variables
=====================

This service will read .env files located at the root of the project. 

### ENV

Set the environment we want to run the service in. Default is Development. 
You can set this to

    - Test
    - Development
    - Production

If ENV is set to Production the database pool will be bigger and the API will log all requests
to a file located in logs/server.log

### PGHOST

Postgres host

### PGPORT

Postgres port

### PGUSER

Postgres user

### PGPASS

Postgres password

### PGDATABASE

Postgres database name

### THJODSKRA_API_KEY

The API key for retrieving data from Ferli. Note that this service uses xml endpoints from Ferli:
http://xml.ferli.is/tjodarsyn/service.asmx?wsdl

Production
==========

You can easily run this service with docker-compose. To do so make sure you have a 
.env.production file located in your root folder. This is the file docker-compose uses
for env variables when running. The options for env variables in this file are identical
to those mentioned above but you might also want to set a postgres password for the
database. You can do so by adding

POSTGRES_PASSWORD=yourpassword

where yourpassword is the password you choose to use. 

Now simply run

```docker-compose up```

to get the service running
