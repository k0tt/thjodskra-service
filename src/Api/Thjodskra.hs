{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE DeriveGeneric     #-}

module Api.Thjodskra where

import Control.Monad.IO.Class      (MonadIO, liftIO)
import Data.Text                   (Text, pack)
import Database.Persist.Postgresql (Entity (..), fromSqlKey, insert, getBy, update
                                   , selectFirst, selectList, (==.), (=.))
import Network.HTTP.Client         (defaultManagerSettings)
import Network.SOAP.Transport.HTTP (initTransportWithM, printRequest, printBody)
import Servant
import System.Environment          (getEnv)

import Config                      (AppT (..))
import Models
import Client                      (getPerson)

type ThjodskraAPI = "thjodskra" :> 
                    "kennitala" :>
                    Capture "kennitala" Text :>
                    Get '[JSON] (Maybe Thjodskra) 

serviceServer :: MonadIO m => ServerT ThjodskraAPI (AppT m)
serviceServer = kennitalaLookup 

kennitalaLookup :: MonadIO m => Text -> AppT m (Maybe Thjodskra)
kennitalaLookup kennitala = do
    p <- runDb $ getBy $ UniqueKennitala kennitala 
    case p of
      Just (Entity personID person) -> return $ Just person
      Nothing -> do
	key <- liftIO $ getEnv "THJODSKRA_API_KEY"
        person <- liftIO $ getPerson kennitala (pack key)
        case thjodskraKennitala person of
            "" -> return Nothing
            _ -> do
                personID <- runDb $ insert person
                return $ Just person
