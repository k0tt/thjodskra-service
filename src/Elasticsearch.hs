{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Elasticsearch where

import Control.Monad               (sequence_, unless)
import Control.Monad.Reader        (MonadIO, MonadReader, asks)
import Data.Aeson
import Database.V5.Bloodhound
import GHC.Generics                (Generic)
import Database.Persist.Postgresql (Entity(..), selectList, fromSqlKey)
import Network.HTTP.Client         (responseStatus)
import Network.HTTP.Types          (statusCode)
import qualified Data.Text         as T

import Models                      (Thjodskra(..), runDb)
import Config                      (Config, esEnv)

data ThjodskraMapping = ThjodskraMapping deriving (Eq, Show, Generic)

instance FromJSON ThjodskraMapping 
instance ToJSON  ThjodskraMapping

indexSettings :: IndexSettings
indexSettings = defaultIndexSettings

indexName :: IndexName
indexName = IndexName "yourindex"

mappingName :: MappingName
mappingName = MappingName "yourmapping"

makeIndex :: (MonadReader Config m, MonadIO m) => m Reply
makeIndex = runES $ createIndex indexSettings indexName

destroyIndex :: (MonadReader Config m, MonadIO m) => m Reply
destroyIndex = runES $ deleteIndex indexName

makeMapping :: (MonadReader Config m, MonadIO m) => m Reply
makeMapping = runES $ putMapping indexName mappingName ThjodskraMapping 

putThjodskra :: (MonadReader Config m, MonadIO m) => Thjodskra -> T.Text -> m Reply
putThjodskra object dID = do
    reply <- getThjodskra dID
    let sCode = statusCode $ responseStatus reply
    case sCode of
      -- update
      200 -> updateThjodskra object dID
      -- create
      404 -> runES $ 
             indexDocument 
             indexName 
             mappingName 
             defaultIndexDocumentSettings 
             object $ DocId dID

updateThjodskra :: (MonadReader Config m, MonadIO m) => Thjodskra -> T.Text -> m Reply
updateThjodskra object dID = runES $ 
                               updateDocument 
                               indexName 
                               mappingName 
                               defaultIndexDocumentSettings 
                               object $ DocId dID

allThjodskras :: (MonadReader Config m, MonadIO m) => m [Entity Thjodskra]
allThjodskras = runDb $ selectList [] []

syncDbThjodskras :: (MonadReader Config m, MonadIO m) => m [Reply]
syncDbThjodskras = do
    exists <- runES $ indexExists indexName
    unless exists $ sequence_ [makeIndex, makeMapping]
    objects <- allThjodskras
    mapped <- mapM (\x -> putThjodskra (entityVal x) (getRecordID x)) objects
    return mapped

getThjodskra :: (MonadReader Config m, MonadIO m) => T.Text -> m Reply
getThjodskra dID = runES $ getDocument indexName mappingName (DocId dID)

getRecordID :: Entity Thjodskra -> T.Text 
getRecordID rec = T.pack $ show $ fromSqlKey $ entityKey rec

runES :: (MonadReader Config m, MonadIO m) => BH m a -> m a
runES query = do
    bhEnv <- asks esEnv
    runBH bhEnv query
