{-# LANGUAGE OverloadedStrings   #-}

module Client where

import           Control.Monad.IO.Class      (liftIO)
import    	 Data.Text    		     (Text, pack)
import qualified Data.Text                   as T
import qualified Data.Text.IO                as T
import    	 Network.HTTP.Client         (defaultManagerSettings)
import    	 Network.SOAP 		     (invokeWS, Transport, 
				  	     ResponseParser(CursorParser))
import    	 Network.SOAP.Transport.HTTP (initTransportWithM, printRequest, printBody)
import    	 Network.SOAP.Parsing.Cursor (Dict, dictBy, readT, readC)
import    	 System.Environment 	     (getEnv)
import           Text.XML.Cursor             (Cursor, content, laxElement, ($/),
                                             (&/), (&//))
import    	 Text.XML.Writer 	     (elementA, element)

import 		 Models           	     (Thjodskra(..))


getPerson :: Text -> Text -> IO Thjodskra
getPerson kt key = do
    t <- initTransportWithM defaultManagerSettings "http://xml.ferli.is/tjodarsyn/service.asmx?wsdl" pure pure
    invokeWS t url () body (CursorParser parser)
    where
        url = "http://xml.ferli.is/GetNafn_Grunn"
        body = elementA "GetNafn_Grunn" [("xmlns", "http://xml.ferli.is")] $ do
            element "str_Ktala" kt
            element "str_PassWord" key
	parser cur = Thjodskra (textOf res "Kennitala")
			       (Just $ textOf res "Nafn")
			       (Just $ textOf res "Heimili")
			       (Just $ textOf res "PostNr" )
			       (Just $ textOf res "Postfang")
			       (Just $ textOf res "FaedDagur")
	    where
	        res = head $ cur $/ laxElement "GetNafn_GrunnResponse" &/ laxElement "GetNafn_GrunnResult"

textOf :: Cursor -> Text -> Text
textOf c t = T.concat $ c $/ laxElement t &// content
